Ensure Molecule is installed

Look for ``molecule``, and if not found, install it via ``pip`` into a
virtual environment for the current user.

**Role Variables**

.. zuul:rolevar:: molecule_version
   :default: 3.6.1

   Molecule version to install on the host.
